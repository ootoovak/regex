# REGEX

An implemention of a Regular Expression engine that deals with wild and optional
characters. This was just done for some practice and is not indended to be
anything aproaching a real regular expression engine.

## Run Tests

This code comes with a simple hand written assert method to run tests. You can
run it using the following command:

```ruby
ruby regex.rb
```

