def assert(result)
  caller_line = caller.first.split(":")[1]
  result_str = result ? "PASS" : "FAIL"
  puts "#{caller_line} : #{result_str}"
  fail unless result
end

class MatchExp
  WILD_CHAR = "."
  OPTIONAL_FLAG = "?"

  def initialize(expr_chars)
    @to_match = expr_chars.shift
    if expr_chars.first == OPTIONAL_FLAG
      expr_chars.shift
      @optional = true
    end
  end

  def match?(value)
    return true if wild?
    value == @to_match
  end

  def wild?
    @to_match == WILD_CHAR
  end

  def optional?
    !!@optional
  end
end

def parse_expr(expr_chars)
  result = []
  result << MatchExp.new(expr_chars) until expr_chars.empty?
  result
end

def match_expr(exprs, value_chars)
  pointer = 0
  results = []

  exprs.each do |expr|
    current_char = value_chars[pointer]
    result = expr.match?(current_char)

    next if !result && expr.optional?

    pointer +=1
    results << result
  end

  results
end

def match(expr, value)
  return false if expr.empty? && !value.empty?
  expr_chars = expr.chars
  value_chars = value.chars
  exprs = parse_expr(expr_chars)
  matches = match_expr(exprs, value_chars)
  matches.all?
end


assert(match("abc", "abc"))
assert(!match("abc", ""))
assert(!match("abc", "a"))
assert(!match("", "abc"))
assert(match("", ""))

assert(match("abc.", "abcd"))
assert(match("a.cd", "abcd"))
assert(!match("a.d", "abcd"))

assert(match("ab?c", "abc"))
assert(match("ab?c", "ac"))
assert(match("ab?cd?", "abc"))
assert(match("ab?c", "ac"))
assert(!match("a?c", "bc"))